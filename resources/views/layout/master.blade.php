<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Title - @yield('title')</title>
    <style>
        h3{
            color: #888;
        }
        h2{
            color: aqua;
        }
        h1{
            color: blueviolet;
        }
    </style>
</head>
<body>
    <header>
        <h1>This is header</h1>
    </header>
    <nav>
        <h2>
            This is nav
        </h2>
    </nav>
    <div class="container">
        @section('maincontent')
        <h1> My name is  
            {{ $name }}
        </h1>
        <h3>This is content</h3>
        @show
    </div>
    <div>
        <h3>This is if statement</h3>
        @if ($time == 'morning')
            <h3>Time to go to school</h3> 
        @else
            <h3>Time to play</h3>               
        @endif
    </div>
    <div>
        <h3>This is Loop Statement</h3>
        @foreach ($days as $days)
            {{$days}}<br/>
        @endforeach
    </div>
    <footer>
        <h1>This is footer</h1>
    </footer>
    

</body>
</html>