<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/bai1',function(){

    $days = ['Monday','Tuesday','Wednesday','Thusday','Friday','Sartuday','Sunday'];
    return view('test.test1',['name' => 'Nguyễn Huy Hoàng', 'time' => 'morning', 'days'=> $days]);
});

Route::get('/bai4','CatagoryController@index');