<!doctype html>
<html>
 
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/fe505d9e24.js" crossorigin="anonymous"></script>    

    <title>View Category</title>
</head>
 
<body>
<div class="container">
    <div class="row">
        <div class="mt-3 col-lg-12"> 
            
            <div class="well">
            
                {!! Form::open(['url' => '/bai4', 'class' => 'form-horizontal']) !!}
            
                <fieldset>
            
                    <legend>Form Input Category</legend>
            
                    <!-- First Number -->
                    <div class="form-group">
                        {!! Form::label('ctg_label', 'Category Name:', ['class' => 'col-lg-2 control-label']) !!}
                        <div class="col-lg-10 mx-auto">
                            {!! Form::text('category_input','', ['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                        
                    <!-- Submit Button -->
                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2 mx-auto">
                            {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-right col-lg-12'] ) !!}
                        </div>
                    </div>

                    <!-- Total -->
                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                        
                        </div>
                    </div>
            
                </fieldset>
            
                {!! Form::close() !!}
            
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Category Name</th>
                <th scope="col">Function</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($categories as $categories)
              <tr>
                <th scope="row">{{ $categories->id }}</th>
                <td>{{ $categories->name }}</td>
                <td>
                        <i class="fas fa-edit"></i>
                        <i class="fas fa-trash-alt"></i>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        
        
       
    </div>
</div>
</body>
 
</html>
